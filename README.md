# Setup

$ mkdir out
$ cd out
$ cmake -G Ninja ..
$ ninja
$ ./some_analyzer_main


# TODO

- forget lv2 for now, just create qt widget with plot functionality first (setting up build system etc...)

# Concept

- record and playback small snippet (stereo)
- inspect sound data in a microscopic way
- record and playback small snippet (stereo)
- show recorded snippet in time domain and frequency domain
- arbitrary resolution wave data in time domain
- compare several snippets (left vs stereo in stereo input) in a meaning way
- lv2 plugin
  - instance access required (or state path and worker will do)

[ ports ]

AudioIn_L
AudioIn_R
AudioOut_L
AudioOut_R

CtrlIn_RecordStart
CtrlIn_RecordStop

CtrlOut_Recording


# Notes

- UI ideas are hugely influenced by polyphone
