#include <QtWidgets/QApplication>
#include "some_plot_wrapper.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  SomePlotWrapper widget;
  widget.show();
  return app.exec();
}
