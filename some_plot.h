#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtWidgets/QWidget>

using namespace QtCharts;

class SomePlot : public QChartView
{
  Q_OBJECT

public:
  explicit SomePlot(QWidget* parent = 0);
  ~SomePlot();
  void loadExampleSamples();
  void loadSamples(float*, int);
  void emitSignals();

private:
  // override
  void mousePressEvent(QMouseEvent*);
  void mouseReleaseEvent(QMouseEvent*);
  void mouseMoveEvent(QMouseEvent*);
  void wheelEvent(QWheelEvent*);

  void updateAxisX(int, int);
  void normalizeChanges(int&, int&);

  // ui states
  bool zooming_horizontal_;
  bool zooming_vertical_;
  QPoint pressed_point_;

  // TODO: stereo and more control
  QChart* chart_;
  QLineSeries* series_;

  int axisx_offset_;
  int axisx_num_samples_; // this is the number of samples currently plotted
  int pressed_axisx_offset_;
  int pressed_axisx_num_samples_;
  int num_samples_;
  float* example_samples_;

public slots:
  void setAxisXOffset(int);

signals:
  void axisXOffsetChanged(int);     // have a direct corresponce to qt abstract slider
  void axisXRangeChanged(int, int);
};
