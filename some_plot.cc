#include <cmath> // M_PI, sin
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QWidget>
#include "some_plot.h"

using namespace QtCharts;

SomePlot::SomePlot(QWidget *parent) :
    QChartView(parent),
    zooming_horizontal_(false), zooming_vertical_(false),
    chart_(new QChart),
    series_(new QLineSeries),
    example_samples_(nullptr) {

  // NOTE: there's quite tangled-up object relationship, hoping memory won't leak (see SomePlot::~SomePlot)
  setChart(chart_);
  chart()->addSeries(series_);
  chart()->setAxisX(new QValueAxis, series_);
  chart()->setAxisY(new QValueAxis, series_);
  chart()->axisX(series_)->hide(); // TODO: show grid and some useful steps
  chart()->axisY(series_)->hide();
  chart()->axisX(series_)->setRange(0, 48000);
  chart()->axisY(series_)->setRange(-1, 1);
  chart()->legend()->hide();

  loadExampleSamples();
}

SomePlot::~SomePlot() {
  // NOTE: I expected delete call sequence something like this:
  // QChartView (QChartViewPrivate) -> QChart -> ChartDataSet -> QAbstractSeries, QAbstractAxis ..
  // but it seems QChartViewPrivate doesn't delete owned QChart (I looked up the version with commit hash 524229e3)
  // so here it is.
  delete chart_;
  delete example_samples_;
}

void SomePlot::loadExampleSamples() {
  int size = 48000;
  if (!example_samples_) {
    int hz = 8;
    example_samples_ = (float*)calloc(size, sizeof(float));
    for (int i = 0; i < size; i++) {
      example_samples_[i] = sin(hz * 2 * M_PI * i / size);
    }
  }
  loadSamples(example_samples_, size);
}

void SomePlot::loadSamples(float* samples, int num_samples) {
  num_samples_ = num_samples;
  QVector<QPointF> vector(num_samples_);
  for (int i = 0; i < num_samples_; i++) {
    vector[i] = QPointF(i, samples[i]);
  }
  series_->replace(vector);

  // initialize axisx
  axisx_offset_ = 0;
  axisx_num_samples_ = num_samples_;
  chart()->axisX(series_)->setRange(0, num_samples_);
  emitSignals();
}

void SomePlot::emitSignals() {
  emit axisXOffsetChanged(axisx_offset_);
  emit axisXRangeChanged(0, num_samples_ - axisx_num_samples_);
}

void SomePlot::normalizeChanges(int& axisx_offset, int& axisx_num_samples) {
  if (num_samples_ < axisx_offset + axisx_num_samples &&
      axisx_num_samples <= num_samples_) {
    axisx_offset = num_samples_ - axisx_num_samples;
  } else if (axisx_offset < 0) {
    if (axisx_num_samples - axisx_offset <= num_samples_) {
      axisx_offset = 0;
    } else {
      axisx_offset = 0;
      axisx_num_samples = num_samples_;
    }
  }
}

void SomePlot::setAxisXOffset(int offset) {
  int axisx_num_samples = axisx_num_samples_;
  normalizeChanges(offset, axisx_num_samples);
  updateAxisX(offset, axisx_num_samples);
}

void SomePlot::updateAxisX(int axisx_offset, int axisx_num_samples) {
  normalizeChanges(axisx_offset, axisx_num_samples);
  if (0 <= axisx_offset &&
      axisx_offset + axisx_num_samples <= num_samples_) {
    if (axisx_offset_ != axisx_offset) {
      axisx_offset_ = axisx_offset;
      emit axisXOffsetChanged(axisx_offset_);
    }
    if (axisx_num_samples_ != axisx_num_samples) {
      axisx_num_samples_ = axisx_num_samples;
      emit axisXRangeChanged(0, num_samples_ - axisx_num_samples_);
    }
    chart()->axisX(series_)->setRange(axisx_offset_, axisx_offset_ + axisx_num_samples_);
  }
}

void SomePlot::mousePressEvent(QMouseEvent* event) {
  pressed_point_  = event->pos();
  pressed_axisx_offset_ = axisx_offset_;
  pressed_axisx_num_samples_ = axisx_num_samples_;
  QRectF rect = chart()->plotArea();
  if (rect.contains(pressed_point_)) {
    zooming_horizontal_ = event->modifiers() & Qt::ControlModifier;
    zooming_vertical_ = event->modifiers() & Qt::ShiftModifier; // TODO: vertical zoom
  }
}

void SomePlot::mouseReleaseEvent(QMouseEvent* event) {
  zooming_vertical_ = zooming_horizontal_ = false;
}

void SomePlot::mouseMoveEvent(QMouseEvent* event) {
  QPoint point = event->pos();
  if (zooming_horizontal_) {
    QRectF rect = chart()->plotArea();
    double ratio = (pressed_point_.x() - rect.x()) / rect.width();
    double diff = (point.x() - pressed_point_.x()) * (pressed_axisx_num_samples_ / rect.width());
    updateAxisX(pressed_axisx_offset_ + diff * ratio, pressed_axisx_num_samples_ - diff);
  } else if (zooming_vertical_) {
    // TODO: vertical zoom
  } else {
    // TODO: mouseover should reveal the point coordinates of current sample
  }
}

void SomePlot::wheelEvent(QWheelEvent* event) {
  QRectF rect = chart()->plotArea();
  double dx = -0.05 * (axisx_num_samples_ / rect.width()) * event->angleDelta().x();
  setAxisXOffset(axisx_offset_ + dx);
}
