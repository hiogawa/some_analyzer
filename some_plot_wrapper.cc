#include <QtWidgets/QWidget>
#include "some_plot_wrapper.h"
#include "ui_some_plot_wrapper.h"

SomePlotWrapper::SomePlotWrapper(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::SomePlotWrapper) {
  ui->setupUi(this);

  // NOTE: emitSignals in SomePlot::loadSamples won't be catched by QScrollBar at that point.
  //       So, here we explicitly signal it.
  ui->somePlot->emitSignals();
}

SomePlotWrapper::~SomePlotWrapper() {
  delete ui;
}
