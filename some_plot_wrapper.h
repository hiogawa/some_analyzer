#include <QtWidgets/QWidget>

namespace Ui {
  class SomePlotWrapper;
}

class SomePlotWrapper : public QWidget
{
  Q_OBJECT

public:
  explicit SomePlotWrapper(QWidget *parent = 0);
  ~SomePlotWrapper();

public slots:
  // void on_plot_data_changed(float*);

private slots:
  // void on_quitButton_clicked();

private:
  Ui::SomePlotWrapper *ui;
};
